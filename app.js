const path = require("path")
const express = require("express")
const logger = require("morgan")
const bodyParser = require("body-parser") // simplifies access to request body
const fs = require('fs')  // NEW - this is required
const app = express()  // make express app
const http = require('http').Server(app)  // inject app into the server
//const port = 8081

// ADD THESE COMMENTS AND IMPLEMENTATION HERE 
// 1 set up the view engine
// 2 manage our entries
// 3 set up the logger
// 4 handle valid GET requests
// 5 handle valid POST request (not required to fully work)
// 6 respond with 404 if a bad URI is requested

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view

// 2 include public assets and use bodyParser
// Node uses __dirname for the The directory name of the current module.
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// 3 log requests to stdout and also
// log HTTP requests to a file using the standard Apache combined format
// see https://github.com/expressjs/morgan for more
var accessLogStream = fs.createWriteStream(__dirname + '/access.log', { flags: 'a' });
app.use(logger('dev'));
app.use(logger('combined', { stream: accessLogStream }));

// 4 http GET default page at /
app.get("/", function (req, res) {
  //res.sendFile(path.join(__dirname + '/assets/index.html'))
  res.render("index.ejs")
})

// 4 http GET /tic-tac-toe
app.get("/tictac", function (req, res) {
  res.render("tictac.ejs")
})

// 4 http GET /about


// 4 http GET /contact
app.get("/contact", function (req, res) {
  res.render("contact.ejs")
})

// 4 http GET /tic-tac-toe
app.get("/Myprogram", function (req, res) {
  res.render("Myprogram.ejs")
})

// 5 http POST /contact
app.post("/contact", function (req, res) {
  var api_key = '85f7897f8ac24054f24460647a0a2338-4836d8f5-ad5c1574';
  var domain = 'sandbox26f7266e41864dab9a24f7d5678b7744.mailgun.org';
  var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
  const firstname = req.body.firstName;
  const lastname = req.body.lastName;
  const email1 = req.body.email;
  const phone = req.body.phone;
  const gender = req.body.gridRadios;
  const comments = req.body.body;
  const isError = true;
  var data = {
    from: 'Great learning experience <postmaster@sandbox26f7266e41864dab9a24f7d5678b7744.mailgun.org>',
    to: 'vishalreddy.pannala@gmail.com',
    subject: lastname + ', ' + firstname + ' - ' + ' has visited your page',
    html: "<b>Email: </b>" + email1 + "<br><b><phone>Phone: </b>" + phone + "<br><b style='color: blue'> Comments: </b>" + comments
  };

  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    // logs to the terminal window (not the browser)
    console.log('\nCONTACT FORM DATA: ' + firstname + ' ' + email1 + ' ' + comments + '\n');
    if (!error) {
      res.send('Thanks for submitting your information. Your information is mailed to me and I will get in touch with you as soon as possible.')
    }else {
      res.send('Your info is not mailed to me.')
    }
  });

})
// 6 this will execute for all unknown URIs not specifically handled
app.get(function (req, res) {
  res.render("404")
})

// Listen for an application request on designated port
app.listen(process.env.PORT || 8081, function () {
  console.log('Web app started and listening on http://localhost:8081/')
})

