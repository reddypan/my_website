QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 == "1", "1=1 success!");
});

QUnit.test('Testing of speeding ticket', function (assert) {
    assert.equal(caughtSpeeding(60, '03/06/1994'), 0, 'Checking with speed 60 and DOB');
    assert.equal(caughtSpeeding(60), 100, 'Checking with speed 60 and without DOB');
    assert.equal(caughtSpeeding(120), 200, 'Checking with speed 60 and without DOB');
    assert.equal(caughtSpeeding(140, '06/05/1994'), 0, 'Checking with speed 60 and without DOB');
    assert.throws(function () { caughtSpeeding(-4); }, new Error('Invalid Speed'), "correctly raises an error if speed is zero or less than zero");

})