var result = document.getElementById('#result')

function caughtSpeeding(speed, isBirthday) {
    let speed1 = parseInt(speed)
    let today = new Date();
    let fine;
    if (isBirthday) {
        if (speed1 <= 65 && speed >= 1) {
            fine = 0;
            
        } else if (speed1 >= 66 && speed1 <= 85) {
            fine = 0;
            
        } else if (speed1 >= 86) {
            fine = 0;
        }
        else if(speed1<=0){
            throw Error("Invalid Speed")
        }
    }
    else {
        if (speed1 <= 60 && speed >= 1) {
            fine = 100;
        } else if (speed1 >= 61 && speed1 <= 80) {
            fine = 150;
        } else if(speed1<=0){
            throw Error("Invalid Speed")
        }
        else {
            fine = 200;
        }
        
    }
    // document.getElementById("result").innerHTML = 'The fine will be: ' + fine;
    return fine;
} 